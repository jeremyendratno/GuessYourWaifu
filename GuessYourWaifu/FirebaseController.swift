//
//  FirebaseController.swift
//  GuessYourWaifu
//
//  Created by Jeremy Endratno on 11/19/21.
//

import Foundation
import FirebaseDatabase

class FirebaseDatabaseController {
    
    func create(ref: DatabaseReference, player: String, score: String) {
        guard let key = ref.child("leaderboard").child(player).child("score").childByAutoId().key else { return }
        let childUpdates = ["leaderboard/\(player)/score/\(key)" : score]
        ref.updateChildValues(childUpdates)
    }
    
    func read(ref: DatabaseReference) -> [(String, Int)] {
        var leaderboard: [(String, Int)] = []
        ref.child("leaderboard").observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            let players = value?.allKeys as! [String]
            
            for player in players {
                let scores = value?.value(forKey: player) as! NSDictionary
                let scoresTemp = scores.value(forKey: "score") as! NSDictionary
                let scoresArray = scoresTemp.allValues as! [String]
                
                for score in scoresArray {
                    leaderboard.append((player, Int(score) ?? 0))
                }
            }
            
            leaderboard.sort {$0.1 > $1.1}
    
        }) { error in
            print(error.localizedDescription)
        }
        
        return leaderboard
    }
}
