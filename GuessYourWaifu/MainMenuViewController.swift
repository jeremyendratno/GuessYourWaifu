//
//  MainMenuViewController.swift
//  GuessYourWaifu
//
//  Created by Jeremy Endratno on 11/3/21.
//

import UIKit
import FirebaseDatabase

class MainMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var bestScoreLabel: UILabel!
    @IBOutlet weak var leaderboardTableView: UITableView!
    
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let ref = Database.database(url: "https://guessyourwaifu-default-rtdb.asia-southeast1.firebasedatabase.app").reference()
    var leaderboard: [(String, Int)] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bestScore()
        leaderboardSetup()
        startButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 19)
        
    }
    
    func leaderboardSetup() {
        leaderboardTableView.delegate = self
        leaderboardTableView.dataSource = self
        ref.child("leaderboard").observeSingleEvent(of: .value, with: { snapshot in
            let value = snapshot.value as? NSDictionary
            let players = value?.allKeys as! [String]
            
            for player in players {
                let scores = value?.value(forKey: player) as! NSDictionary
                let scoresTemp = scores.value(forKey: "score") as! NSDictionary
                let scoresArray = scoresTemp.allValues as! [String]
                
                for score in scoresArray {
                    self.leaderboard.append((player, Int(score) ?? 0))
                }
            }
            
            self.leaderboard.sort {$0.1 > $1.1}
            self.leaderboardTableView.reloadData()
    
        }) { error in
            print(error.localizedDescription)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if leaderboard.count != 0 {
            return 5
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = leaderboardTableView.dequeueReusableCell(withIdentifier: "LeaderboardCell") as! LeaderboardTableViewCell
        cell.setCell(name: leaderboard[indexPath.row].0, score: String(leaderboard[indexPath.row].1))
        return cell
    }
    
    func bestScore() {
        let bestScore = UserDefaults.standard.string(forKey: "Best Score")
        bestScoreLabel.text = "Best Score: \(bestScore ?? "0")"
    }
    
    @IBAction func startButton(_ sender: Any) {
        let guessVC = mainStoryboard.instantiateViewController(withIdentifier: "Guess")
        guessVC.modalPresentationStyle = .fullScreen
        show(guessVC, sender: self)
    }
}

class LeaderboardTableViewCell: UITableViewCell {
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCell(name: String, score: String) {
        nameLabel.text = name
        scoreLabel.text = score
    }
}
