//
//  WaifusDatabase.swift
//  GuessYourWaifu
//
//  Created by Jeremy Endratno on 11/3/21.
//

import Foundation

struct Waifu {
    let name: String
    let image: String
}

class WaifusDatabase {
    
    let waifus: [Waifu] = [
    Waifu(name: "Asuna Yuuki", image: "Asuna Yuuki"),
    Waifu(name: "Mikasa Ackerman", image: "Mikasa Ackerman"),
    Waifu(name: "Konata Izumi", image: "Konata Izumi"),
    Waifu(name: "Saber", image: "Saber"),
    Waifu(name: "Haruhi Suzumiya", image: "Haruhi Suzumiya"),
    Waifu(name: "Hitagi Senjougahara", image: "Hitagi Senjougahara"),
    Waifu(name: "Holo", image: "Holo"),
    Waifu(name: "Taiga Aisaka", image: "Taiga Aisaka"),
    Waifu(name: "Kurisu Makise", image: "Kurisu Makise"),
    Waifu(name: "Yuno Gasai", image: "Yuno Gasai") ]
    
    func getWaifus() -> [Waifu] {
        return waifus
    }
}
