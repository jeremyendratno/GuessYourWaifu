//
//  GuessViewController.swift
//  GuessYourWaifu
//
//  Created by Jeremy Endratno on 11/3/21.
//

import UIKit
import FirebaseDatabase

class GuessViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var waifuImage: UIImageView!
    @IBOutlet weak var guessTextField: UITextField!
    @IBOutlet weak var guessButton: UIButton!
    @IBOutlet weak var scoreTextView: UILabel!
    
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    var ref: DatabaseReference! 
    
    var waifus = WaifusDatabase().getWaifus()
    var round = 0
    var score = 0
    var correct = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldSetup()
        guessButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        
        waifus.shuffle()
        displayWaifu()
        
        databaseSetup()
    }
    
    func databaseSetup() {
        ref = Database.database(url: "https://guessyourwaifu-default-rtdb.asia-southeast1.firebasedatabase.app").reference()
    }
    
    func goToMainMenu() {
        let mainMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "Main Menu")
        mainMenuVC.modalPresentationStyle = .fullScreen
        show(mainMenuVC, sender: self)
    }
    
    func alertSetup() {
        let endingAlert = UIAlertController(title: "You Lose!", message: "Your Score: \(score)", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Okay", style: .default) {_ in
            self.alertLeaderboardSetup()
        }
        endingAlert.addAction(okAction)
        present(endingAlert, animated: true, completion: nil)
    }
    
    func alertLeaderboardSetup() {
        let alert = UIAlertController(title: "Enter Your Name", message: "Your score will be displayed on the leaderboard across the world", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Your Name"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let player = alert?.textFields![0]
            FirebaseDatabaseController().create(ref: self.ref, player: player?.text ?? "Annonymous", score: String(self.score))
            self.goToMainMenu()
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    func viewSetup() {
        waifuImage.layer.cornerRadius = 15
    }
    
    func textFieldSetup() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tap)
        guessTextField.delegate = self
    }
    
    func displayWaifu() {
        waifuImage.image = UIImage(named: waifus[round].image)
    }
    
    func guessing() {
        let waifuNameComponents = waifus[round].name.lowercased().components(separatedBy: " ")
        for names in waifuNameComponents {
            if names == guessTextField.text?.lowercased() {
                score += 100
                correct = true
                break
            }
        }
        
        if correct == true {
            scoreTextView.text = "Score: \(score)"
            round += 1
            
            if round == waifus.count {
                round = 0
                waifus.shuffle()
            }
            
            correct = false
            displayWaifu()
        } else {
            let bestScore = UserDefaults.standard.string(forKey: "Best Score")
            if Int(bestScore ?? "0") ?? 0 < score {
                UserDefaults.standard.set("\(score)", forKey: "Best Score")
            }
            alertSetup()
        }
        
        
    }
    
    @IBAction func guessButton(_ sender: Any) {
        guessing()
        guessTextField.text?.removeAll()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func handleTap() {
        guessTextField.resignFirstResponder()
    }
}
